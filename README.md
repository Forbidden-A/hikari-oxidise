<h1 align="center">Hikari Oxidise</h1>
<p align="center">
A <a href="https://hikari-py.dev">Hikari</a> extension providing a Command Handler, Component Handler, and other utilities.
</p>

## Disclaimer
This project is made for fun and
for me to learn more Rust and Python,
if I end up with something useful I will
keep it maintained but don't get your hopes up.

## Documentation

[TODO](#TODO)

## Usage

### Installation
[TODO](#TODO)

<div class="warning">
<br>
<strong>NOTICE:</strong>
<br>
It is highly recommended that you activate a <a href="https://docs.python.org/3/tutorial/venv.html">Python Virtual Environment</a>
before running any of the following commands.
<br>
</div>

```shell
python -m pip insall git+https://gitlab.com/Forbidden-A/hikari-oxidise@current
```

### Creating a Bot
[TODO](#TODO)

## TODO

<div class="todolist">

<div class="todo-column">

<details>
<summary> 🧑‍💻 Complete</summary>

- [x] readme.md base :see_no_evil:

</details>
</div>

<div class="todo-column">
<details>
<summary> 🏗️ Incomplete</summary>

- [ ] Command Handler base
- [ ] Component Handler base
- [ ] Simple Prefix commands
- [ ] Converters
- [ ] Utils
- [ ] Logging

</details>
</div>

</div>